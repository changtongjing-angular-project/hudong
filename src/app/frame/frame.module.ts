/**
 * Created by Administrator on 2018/5/4.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {FrameRoutingModule} from './frame.routes';
import { AuthorizationManagementComponent } from './authorization-management/authorization-management.component';
import { AnalyticalFeedbackComponent } from './analytical-feedback/analytical-feedback.component';
import { HomeComponent } from './home/home.component';
import {AuthService} from '../services/auth-guard.service';

@NgModule({
  declarations: [
    AuthorizationManagementComponent,
    AnalyticalFeedbackComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    FrameRoutingModule
  ],
  entryComponents: [],
  providers: [
    AuthService
  ]
})
export class FrameModule {}
