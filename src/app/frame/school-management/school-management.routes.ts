/**
 * Created by Administrator on 2018/5/4.
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SchoolManagementComponent} from './school-management.component';
import {TeacherComponent} from './teacher/teacher.component';
import {StudentComponent} from './student/student.component';
import {PrincipalComponent} from './principal/principal.component';
import {LeaderComponent} from './leader/leader.component';
import {AuthService} from '../../services/auth-guard.service';

const schoolManagementRoutes: Routes = [
  {
    path: '',
    redirectTo: 'principal',
    pathMatch: 'full'
  },
  {
    path: '',
    component: SchoolManagementComponent,
    children: [
      {
        path: 'principal',
        component: PrincipalComponent
      },
      {
        path: 'teacher',
        component: TeacherComponent
      },
      {
        path: 'student',
        component: StudentComponent
      },
      {
        path: 'leader',
        component: LeaderComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(schoolManagementRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class SchoolManagementRoutingModule {}
