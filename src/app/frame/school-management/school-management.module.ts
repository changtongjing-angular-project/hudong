/**
 * Created by Administrator on 2018/5/4.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {TeacherComponent} from './teacher/teacher.component';
import {StudentComponent} from './student/student.component';
import {PrincipalComponent} from './principal/principal.component';
import {SchoolManagementRoutingModule} from './school-management.routes';
import {SchoolManagementComponent} from './school-management.component';
import { LeaderComponent } from './leader/leader.component';
@NgModule({
  declarations: [
    SchoolManagementComponent,
    TeacherComponent,
    StudentComponent,
    PrincipalComponent,
    LeaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    SchoolManagementRoutingModule
  ],
  entryComponents: [],
  providers: []
})

export class SchoolManagementModule {}
