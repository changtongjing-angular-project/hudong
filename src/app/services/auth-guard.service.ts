/**
 * Created by Administrator on 2018/5/6.
 */
import { Injectable } from '@angular/core';
import {CanActivate, CanActivateChild, CanDeactivate, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthService implements CanActivate, CanActivateChild, CanDeactivate<any> {
  url = '';
  urlTwo = '';
  constructor(private router: Router) {}

  canActivate(route?: any, state?: any): Observable<boolean> | any {
    // 这里判断登录状态, 返回 true 或 false
    const num = Math.random();
    const loggedIn: boolean = num < 0.5;
    if (!loggedIn) {
      // console.log('用户登录失败');
      return this.checkLogin();
    }
    // console.log('用户登录成功');
    return loggedIn;
  }

  canActivateChild(route?: any, state?: any): Observable<boolean> | boolean {
    // window.location.href = window.localStorage.getItem('url');
    // const url = window.location.href + '/principal';
    // console.log(url);
    return true;
  }

  checkLogin() {
    this.url = window.localStorage.getItem('url');
    if (this.url !== '' && this.url !== null) {
      this.router.navigate([window.localStorage.getItem('url').substr(21)]);
    } else {
      this.router.navigate(['/login']);
    }
    // this.router.navigate([window.localStorage.getItem('url').substr(21) + '/principal']);
  }

  canDeactivate(component?: any): Observable<boolean> | any {
    return window.confirm('确定要离开吗');
  }

}
