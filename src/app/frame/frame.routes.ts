/**
 * Created by Administrator on 2018/5/4.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthorizationManagementComponent} from './authorization-management/authorization-management.component';
import {AnalyticalFeedbackComponent} from './analytical-feedback/analytical-feedback.component';
import {FrameComponent} from './frame.component';
import {HomeComponent} from './home/home.component';
import {AuthService} from '../services/auth-guard.service';

const frameRoutes: Routes = [
  {
    path: '',
    redirectTo: '/frame/home',
    pathMatch: 'full'
  },
  {
    path: 'frame/home',
    redirectTo: '/frame/home',
    pathMatch: 'full'
  },
  {
    path: 'frame',
    component: FrameComponent,
    canActivate: [AuthService],
    canDeactivate: [AuthService],
    children: [
      {
        path: 'home',
        component: HomeComponent,
        pathMatch: 'full'
      },
      {
        path: 'authManagement',
        component: AuthorizationManagementComponent
      },
      {
        path: 'analFeedback',
        component: AnalyticalFeedbackComponent
      },
      {
        path: 'schoolManagement',
        loadChildren: 'app/frame/school-management/school-management.module#SchoolManagementModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(frameRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class FrameRoutingModule {}
