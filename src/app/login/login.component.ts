import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NzMessageService, NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  url = '';
  urlTwo = '';
  validateForm: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[ i ].markAsDirty();
      this.validateForm.controls[ i ].updateValueAndValidity();
    }
  }

  constructor(private fb: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [ null, [ Validators.required ] ],
      password: [ null, [ Validators.required ] ],
      remember: [ true ]
    });
  }

  loginIn() {
    this.url = window.localStorage.getItem('url');
    if (this.url !== '' && this.url !== null) {
      this.urlTwo = this.url.substr(21);
      this.router.navigate([this.urlTwo]);
    } else {
      this.router.navigate(['/frame/home']);
    }
  }

}
