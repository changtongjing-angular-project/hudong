import {Component, ElementRef, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styleUrls: ['./frame.component.css']
})
export class FrameComponent implements OnInit {

  url = '';
  urlTwo = '';
  urlThree = '';

  constructor(private el: ElementRef, private router: Router, @Inject(ActivatedRoute) private activateRouter: ActivatedRoute) { }

  ngOnInit() {
    this.url = window.localStorage.getItem('url');
    console.log(this.url);
    if (this.url !== '' && this.url !== null) {
      this.urlTwo = this.url.substr(21);
      // console.log(this.urlTwo);
      // console.log(typeof this.urlTwo);
      if (this.urlTwo === '/frame/home') {
        this.activeBtn(0);
      } else if (this.urlTwo === '/frame/authManagement') {
        this.activeBtn(1);
      } else if (this.urlTwo === '/frame/analFeedback') {
        this.activeBtn(2);
      } else if (this.urlTwo === '/frame/schoolManagement') {
        this.activeBtn(3);
      } else {
        this.urlThree = this.urlTwo.substr(23, 99);
        console.log(this.urlThree);
        if (this.urlThree === '/principal' || this.urlThree === '/teacher' || this.urlThree === '/student' || this.urlThree === '/leader') {
          this.activeBtn(3);
        }
      }
    } else {
      this.activeBtn(0);
    }
  }

  activeBtn(num?: any) {
    if (num === 0) {
      this.el.nativeElement.querySelector('.li-one').style.backgroundColor = '#57b4ff';
      this.el.nativeElement.querySelector('.li-one').style.color = 'white';
      if (window.innerWidth > 900) {
        this.el.nativeElement.querySelector('.li-one').style.fontSize = '18px';
      }
      this.el.nativeElement.querySelector('.li-two').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-two').style.color = '#333';
      this.el.nativeElement.querySelector('.li-two').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-three').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-three').style.color = '#333';
      this.el.nativeElement.querySelector('.li-three').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-four').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-four').style.color = '#333';
      this.el.nativeElement.querySelector('.li-four').style.fontSize = '14px';
    } else if (num === 1) {
      this.el.nativeElement.querySelector('.li-one').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-one').style.color = '#333';
      this.el.nativeElement.querySelector('.li-one').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-two').style.backgroundColor = '#57b4ff';
      this.el.nativeElement.querySelector('.li-two').style.color = 'white';
      if (window.innerWidth > 900) {
        this.el.nativeElement.querySelector('.li-two').style.fontSize = '18px';
      }
      this.el.nativeElement.querySelector('.li-three').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-three').style.color = '#333';
      this.el.nativeElement.querySelector('.li-three').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-four').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-four').style.color = '#333';
      this.el.nativeElement.querySelector('.li-four').style.fontSize = '14px';
    } else if (num === 2) {
      this.el.nativeElement.querySelector('.li-one').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-one').style.color = '#333';
      this.el.nativeElement.querySelector('.li-one').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-two').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-two').style.color = '#333';
      this.el.nativeElement.querySelector('.li-two').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-three').style.backgroundColor = '#57b4ff';
      this.el.nativeElement.querySelector('.li-three').style.color = 'white';
      if (window.innerWidth > 900) {
        this.el.nativeElement.querySelector('.li-three').style.fontSize = '18px';
      }
      this.el.nativeElement.querySelector('.li-four').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-four').style.color = '#333';
      this.el.nativeElement.querySelector('.li-four').style.fontSize = '14px';
    } else if (num === 3) {
      this.el.nativeElement.querySelector('.li-one').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-one').style.color = '#333';
      this.el.nativeElement.querySelector('.li-one').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-two').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-two').style.color = '#333';
      this.el.nativeElement.querySelector('.li-two').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-three').style.backgroundColor = 'white';
      this.el.nativeElement.querySelector('.li-three').style.color = '#333';
      this.el.nativeElement.querySelector('.li-three').style.fontSize = '14px';
      this.el.nativeElement.querySelector('.li-four').style.backgroundColor = '#57b4ff';
      this.el.nativeElement.querySelector('.li-four').style.color = 'white';
      if (window.innerWidth > 900) {
        this.el.nativeElement.querySelector('.li-four').style.fontSize = '18px';
      }
    } else {}
  }

  out() {
    // window.location.href = this.host + '/endsession';
    console.log('退出:' + window.location.href);
    window.localStorage.setItem('url', window.location.href); // 保存url
    this.router.navigate(['/login']);
  }

}
